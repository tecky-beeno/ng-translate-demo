import { Component } from '@angular/core'
import { Lang, TranslateService } from './translate.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ng-translate-demo'
  constructor(private translateService: TranslateService) {}

  setLang(lang: Lang) {
    this.translateService.setLang(lang)
  }
}
