import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { en } from './lang.en'
import { zh } from './lang.zh'
import { map } from 'rxjs/operators'

let words = {
  en,
  zh,
}

export type Lang = keyof typeof words

@Injectable({
  providedIn: 'root',
})
export class TranslateService {
  private langSubject = new BehaviorSubject<Lang>(this.getLang())

  private getLang(): Lang {
    let lang = localStorage.getItem('lang')
    if (lang && lang in words) {
      return lang as Lang
    } else {
      return 'zh'
    }
  }

  constructor() {}

  setLang(lang: Lang) {
    this.langSubject.next(lang)
    localStorage.setItem('lang', lang)
  }

  observeLang(): Observable<Lang> {
    return this.langSubject.asObservable()
  }

  observeWords() {
    return this.observeLang().pipe(map(lang => words[lang]))
  }
}
