import {
  ChangeDetectorRef,
  Injector,
  OnDestroy,
  Pipe,
  PipeTransform,
} from '@angular/core'
import { Observable, Subscription } from 'rxjs'
import { map } from 'rxjs/operators'
import { Lang } from './lang'
import { TranslateService } from './translate.service'

@Pipe({
  name: 'translate',
  pure: false,
})
export class TranslatePipe implements PipeTransform, OnDestroy {
  private sub?: Subscription
  private value?: string
  private key?: keyof Lang

  constructor(
    private translateService: TranslateService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnDestroy(): void {
    this.sub?.unsubscribe()
  }

  transform(key: keyof Lang, ...args: unknown[]): string | undefined {
    if (this.key !== key) {
      delete this.value
      this.sub?.unsubscribe()
      this.sub = this.translateService
        .observeWords()
        .pipe(
          map(words => {
            let value = words[key]
            this.value = value
            this.changeDetectorRef.markForCheck()
          }),
        )
        .subscribe()
    }
    return this.value
  }
}
